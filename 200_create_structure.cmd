echo off
Set SQL_PATH=./sql/
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%290_drop_table_cascade.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%291_purge_recyclebin
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%200_create_table.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%210_create_pk.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%215_create_table_iot.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%220_create_fk.sql
REM sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%230_create_check.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%240_create_sequence.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%250_create_trigger.sql
REM sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%260_verify_structure.sql

echo Structure installée !
