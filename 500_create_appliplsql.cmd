echo off
Set SQL_PATH=./sql/

REM sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%500_create_pqi_param_common.sql
REM sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%500_create_pqb_param_common.sql

REM Common & Utils
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%510_create_pqi_ui_common.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%510_create_pqb_ui_common.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%520_create_pqi_db_utils.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%520_create_pqb_db_utils.sql

REM DB
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%700_create_pqi_db_adresse_mail.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%700_create_pqb_db_adresse_mail.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%710_create_pqi_db_caracteriser.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%710_create_pqb_db_caracteriser.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%720_create_pqi_db_concerner.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%720_create_pqb_db_concerner.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%730_create_pqi_db_contact.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%730_create_pqb_db_contact.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%740_create_pqi_db_evenement.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%740_create_pqb_db_evenement.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%750_create_pqi_db_jour.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%750_create_pqb_db_jour.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%760_create_pqi_db_localisation.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%760_create_pqb_db_localisation.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%770_create_pqi_db_occurence.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%770_create_pqb_db_occurence.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%780_create_pqi_db_partager.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%780_create_pqb_db_partager.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%790_create_pqi_db_periodicite.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%790_create_pqb_db_periodicite.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%800_create_pqi_db_role_utilisateur.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%800_create_pqb_db_role_utilisateur.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%810_create_pqi_db_utilisateur.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%810_create_pqb_db_utilisateur.sql

REM UI
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%600_create_pqi_ui_authentification.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%600_create_pqb_ui_authentification.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%610_create_pqi_ui_calendar.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%610_create_pqb_ui_calendar.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%620_create_pqi_ui_contact.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%620_create_pqb_ui_contact.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%630_create_pqi_ui_event.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%630_create_pqb_ui_event.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%640_create_pqi_ui_user.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%640_create_pqb_ui_user.sql

REM APEX
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%900_create_apex_workspace.sql
sqlplus G06_WEBCAL/WEBCAL @%SQL_PATH%910_create_apex_admin.sql

echo PLSQL App installé !
