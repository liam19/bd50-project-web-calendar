CREATE OR REPLACE PACKAGE PA_EVENEMENT
AS
    -- Ref Cursor
    TYPE c_type_evenement IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numperiod IN NUMBER, in_numuser IN NUMBER,
		in_titreevt IN VARCHAR2, in_dispoevt IN NUMBER, in_descrevt IN VARCHAR2);

    -- Get
    -- FUNCTION Get(in_adrmail IN VARCHAR2) RETURN caracteriser%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_evenement;
	
	-- UPDATE
	PROCEDURE Upd(
        in_numevt IN NUMBER, in_numperiod IN NUMBER, in_numuser IN NUMBER,
		in_titreevt IN VARCHAR2, in_dispoevt IN NUMBER, in_descrevt IN VARCHAR2);

    -- Delete
    PROCEDURE Del(in_numevt IN NUMBER);
	
	--EXISTS
	FUNCTION Exist(in_numevt IN NUMBER) RETURN BOOLEAN;
END;
/

exit;
