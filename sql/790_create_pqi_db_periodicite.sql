CREATE OR REPLACE PACKAGE PA_PERIODICITE
AS
    -- Ref Cursor
    TYPE c_type_periodic IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_typeperiod IN VARCHAR2, in_debutperiodic IN DATE, in_finperiodic IN DATE, in_nboccur IN NUMBER, in_freqperiod IN NUMBER);

    -- Get
    FUNCTION Get(in_numperiod IN NUMBER) RETURN periodicite%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_periodic;

    -- Update
    PROCEDURE Upd(
        in_numperiod IN NUMBER, in_typeperiod IN VARCHAR2, in_debutperiodic IN DATE, 
        in_finperiodic IN DATE, in_nboccur IN NUMBER, in_freqperiod IN NUMBER
    );

    -- Delete
    PROCEDURE Del(in_numperiod IN NUMBER);
END;
/

exit;
