create or replace
PACKAGE PA_PARTAGER
AS
    -- Ref Cursor
    TYPE c_type_partager IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER, in_statprt IN VARCHAR2);

    -- Get
    /*FUNCTION Get(in_adrmail IN VARCHAR2) RETURN adresse_mail%ROWTYPE;*/

    -- GetAll
    FUNCTION GetAll RETURN c_type_partager;

    -- Update
    PROCEDURE Upd(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER, in_statprt IN VARCHAR2);

    -- Delete
    PROCEDURE Del(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER);

    -- Exists
    FUNCTION Exist(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER) RETURN BOOLEAN;
END;
/

exit;
