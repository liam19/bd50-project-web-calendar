CREATE OR REPLACE PACKAGE BODY PA_LOCALISATION
AS
    -- Insert
    PROCEDURE Add(in_numloc IN NUMBER, in_descrloc IN VARCHAR2, in_rueloc IN VARCHAR2, in_cploc IN VARCHAR2, in_villeloc IN VARCHAR2, in_paysloc IN VARCHAR2)
    IS
    BEGIN
        INSERT INTO
            localisation
        VALUES
            (in_numloc, in_descrloc, in_rueloc, in_cploc, in_villeloc, in_paysloc);

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
    -- FUNCTION Get(in_adrmail IN VARCHAR2) RETURN caracteriser%ROWTYPE;

    -- GetAll
    FUNCTION GetAll
    RETURN c_type_local
    IS
        c_local c_type_local;
    BEGIN
        -- select all data from caracteriser
        OPEN c_local FOR SELECT * FROM localisation;
        RETURN c_local;
    END;
	
	-- Update
    PROCEDURE Upd(
        in_numloc IN NUMBER, in_descrloc IN VARCHAR2, in_rueloc VARCHAR2,
		in_cploc VARCHAR2, in_villeloc VARCHAR2, in_paysloc VARCHAR2
    ) IS
    BEGIN
        UPDATE localisation
        SET 
            numloc = in_numloc, descrloc = in_descrloc, rueloc = in_rueloc,
			cploc = in_cploc, villeloc = in_villeloc, paysloc = in_paysloc
        WHERE numloc = in_numloc;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
	
	
	-- Exists
    FUNCTION Exist(in_numloc IN NUMBER)
    RETURN BOOLEAN
    IS
        result INTEGER;
    BEGIN
        SELECT COUNT(*) INTO result FROM localisation
		WHERE numloc = in_numloc;
        IF result > 0 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;

    -- Delete
    PROCEDURE Del(in_numloc IN NUMBER)
    IS
    BEGIN
        DELETE FROM localisation
            WHERE numloc = in_numloc;

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
END;
/

exit;
