CREATE OR REPLACE PACKAGE PA_LOCALISATION
AS
    -- Ref Cursor
    TYPE c_type_local IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numloc IN NUMBER, in_descrloc IN VARCHAR2, in_rueloc VARCHAR2, in_cploc VARCHAR2, in_villeloc VARCHAR2, in_paysloc VARCHAR2);

    -- Get
    --FUNCTION Get(in_adrmail IN VARCHAR2) RETURN caracteriser%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_local;
	
	-- Insert
    PROCEDURE Upd(in_numloc IN NUMBER, in_descrloc IN VARCHAR2, in_rueloc VARCHAR2, in_cploc VARCHAR2, in_villeloc VARCHAR2, in_paysloc VARCHAR2);

    -- Delete
    PROCEDURE Del(in_numloc IN NUMBER);
	
	--EXISTS
	FUNCTION Exist(in_numloc IN NUMBER) RETURN BOOLEAN;
END;
/

exit;
