create or replace PACKAGE BODY PA_UTILS 
AS

  g_key     RAW(32767)  := UTL_RAW.cast_to_raw('12345678');
  g_pad_chr VARCHAR2(1) := '~';

  PROCEDURE padstring (p_text  IN OUT  VARCHAR2);

  FUNCTION encrypt (p_text  IN  VARCHAR2) RETURN RAW IS
    l_text       VARCHAR2(32767) := p_text;
    l_encrypted  RAW(32767);
  BEGIN
    padstring(l_text);
    DBMS_OBFUSCATION_TOOLKIT.desencrypt(input          => UTL_RAW.cast_to_raw(l_text),
                                        key            => g_key,
                                        encrypted_data => l_encrypted);
    RETURN l_encrypted;
  END;


  -- decrypt a password
  FUNCTION decrypt (p_raw  IN  RAW) RETURN VARCHAR2 IS
    l_decrypted  VARCHAR2(32767);
  BEGIN
    DBMS_OBFUSCATION_TOOLKIT.desdecrypt(input => p_raw,
                                        key   => g_key,
                                        decrypted_data => l_decrypted);
                                        
    RETURN RTrim(UTL_RAW.cast_to_varchar2(l_decrypted), g_pad_chr);
  END;


  PROCEDURE padstring (p_text  IN OUT  VARCHAR2) IS
    l_units  NUMBER;
  BEGIN
    IF LENGTH(p_text) MOD 8 > 0 THEN
      l_units := TRUNC(LENGTH(p_text)/8) + 1;
      p_text  := RPAD(p_text, l_units * 8, g_pad_chr);
    END IF;
  END;

	PROCEDURE set_langage_fr IS
	BEGIN
	 
		execute immediate 'ALTER SESSION SET NLS_LANGUAGE=''FRENCH''';
		execute immediate 'ALTER SESSION SET NLS_TERRITORY=''FRANCE''';
		execute immediate 'ALTER SESSION SET NLS_CALENDAR=''GREGORIAN''';
		execute immediate 'ALTER SESSION SET NLS_DATE_FORMAT=''DD.MM.RRRR''';
		execute immediate 'ALTER SESSION SET NLS_DATE_LANGUAGE=''FRENCH''';
		
	END;

  -- get connect user via cookie
	FUNCTION get_user_connected 
  RETURN utilisateur%rowtype 
  IS
    cursor req_utilisateur IS
      SELECT *
      FROM utilisateur UTL
      WHERE UTL.pseudouser = get_cookie('login')
      AND UTL.mdpuser = get_cookie('mdp');

    usr utilisateur%rowtype;  
  BEGIN
    OPEN req_utilisateur;
    FETCH req_utilisateur INTO usr;
    
    if req_utilisateur%NOTFOUND then
      return null;
    end if;
    return usr;
    close req_utilisateur;

	END;

    PROCEDURE disconnect_user AS
    BEGIN
    -- remove cookie
		remove_cookie;
		
		-- redirect
		ui_common.redirect('ui_auth.formconnectuser');
    END;

  -- remove cookie
	procedure remove_cookie is
  begin
		owa_util.mime_header('text/html',false);
		owa_cookie.remove('login',null);
		owa_cookie.remove('mdp',null);
		owa_util.http_header_close;
  end;

  -- get cookie by name
  function get_cookie(v_name in varchar2) return varchar2 is
		v_cookie_val owa_cookie.cookie;
	begin
		v_cookie_val := owa_cookie.get(v_name);
		if v_cookie_val.num_vals != 0 then
			return(v_cookie_val.vals(1));
		else
			return null;
		end if;
	end get_cookie;

  -- set a cookie
  procedure set_cookie
	  (v_login in varchar2, v_mdp in varchar2)
	is
  begin
		owa_util.mime_header('text/html',false);
		owa_cookie.send('login', v_login); --, pexpires DATE
		owa_cookie.send('mdp', v_mdp);
		owa_util.http_header_close;
  end;

  -- check email adress with REGEX 
  FUNCTION check_mail(v_mail IN VARCHAR2) 
  RETURN BOOLEAN
  IS
  BEGIN
    RETURN REGEXP_LIKE(v_mail, '\w+@\w+(\.\w+)+');
  END;
  
  -- check phone number with REGEX
  FUNCTION check_tel(v_tel  IN VARCHAR2) 
  RETURN BOOLEAN
  IS
  BEGIN
    RETURN REGEXP_LIKE(v_tel, '\d{10,10}');
  END;
 
END;
/

exit;
