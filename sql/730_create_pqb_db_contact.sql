CREATE OR REPLACE PACKAGE BODY PA_CONTACT
AS
    -- Insert
    PROCEDURE Add(in_numuser IN NUMBER, in_numcontact IN NUMBER)
    IS
    BEGIN
        INSERT INTO
            contact
        VALUES
            (in_numuser, in_numcontact);

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
    FUNCTION Get(in_numuser IN NUMBER, in_numcontact IN NUMBER) 
    RETURN contact%ROWTYPE
    IS
        l_contact contact%ROWTYPE;
    BEGIN
        SELECT CON.* 
        INTO l_contact 
        FROM contact CON 
        WHERE CON.numuser = in_numuser
        AND CON.numcontact = in_numcontact;

        RETURN l_contact;
    END;

    -- GetAll
    FUNCTION GetAll
    RETURN c_type_contact
    IS
        c_contact c_type_contact;
    BEGIN
        -- select all data from contact
        OPEN c_contact FOR SELECT * FROM contact;
        RETURN c_contact;
    END;

    -- Get All Contacts
    FUNCTION GetAllContacts(in_numuser IN NUMBER) 
    RETURN c_type_contact
    IS
        c_contact c_type_contact;
    BEGIN
        OPEN c_contact 
        FOR 
            SELECT
                UTL. *
            FROM
                utilisateur UTL
                INNER JOIN contact CON ON UTL.numuser = CON.numcontact
            WHERE
                CON.numuser = in_numuser;
        RETURN c_contact;
    END;

    -- Delete
    PROCEDURE Del(in_numuser IN NUMBER, in_numcontact IN NUMBER)
    IS
    BEGIN
        DELETE FROM contact
            WHERE numuser = in_numuser AND numcontact = in_numcontact;

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
	
	-- Exists
    FUNCTION Exist(in_numuser IN NUMBER, in_numcontact IN NUMBER)
    RETURN BOOLEAN
    IS
        result INTEGER;
    BEGIN
        SELECT COUNT(*) INTO result FROM contact 
		WHERE numuser = in_numuser AND numcontact = in_numcontact;
        IF result > 0 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;
END;
/

exit;
