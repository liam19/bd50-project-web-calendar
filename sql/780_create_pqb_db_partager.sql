create or replace
PACKAGE BODY PA_PARTAGER
AS
    -- Insert
    PROCEDURE Add(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER, in_statprt IN VARCHAR2)
    IS
    BEGIN
        INSERT INTO
            partager
        VALUES
            (in_numuser, in_numevt, in_numoccur, in_statprt);

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
   /* FUNCTION Get(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER) 
    RETURN partager%ROWTYPE
    IS
        l_mail partager%ROWTYPE;
    BEGIN
        SELECT ADR.* INTO l_mail FROM adresse_mail ADR WHERE ADR.adrmail = in_adrmail;
        RETURN l_mail;
    END;*/

    -- GetAll
    FUNCTION GetAll
    RETURN c_type_partager
    IS
        c_partager c_type_partager;
    BEGIN
        -- select all data from partager
        OPEN c_partager FOR SELECT * FROM partager;
        RETURN c_partager;
    END;

    -- Update
    PROCEDURE Upd(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER, in_statprt IN VARCHAR2)    
    IS
    BEGIN
        UPDATE partager
        SET statprt = in_statprt
        WHERE numuser = in_numuser AND numevt = in_numevt AND numoccur = in_numoccur;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Delete
    PROCEDURE Del(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER)
    IS
    BEGIN
        DELETE FROM partager
        WHERE numuser = in_numuser AND numevt = in_numevt AND numoccur = in_numoccur;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Exists
    FUNCTION Exist(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER)
    RETURN BOOLEAN
    IS
        result INTEGER;
    BEGIN
        SELECT COUNT(*) INTO result FROM partager 
		WHERE numuser = in_numuser AND numevt = in_numevt AND numoccur = in_numoccur;
        IF result > 0 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;
END;
/

exit;
