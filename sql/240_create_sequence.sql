-- -----------------------------------------------------------------------------
--       TABLE : UTILISATEUR
-- -----------------------------------------------------------------------------
CREATE SEQUENCE utilisateur_seq
START WITH
    1 INCREMENT BY 1 NOCACHE NOCYCLE;

-- -----------------------------------------------------------------------------
--       TABLE : THEME
-- -----------------------------------------------------------------------------
CREATE SEQUENCE theme_seq
START WITH
    1 INCREMENT BY 1 NOCACHE NOCYCLE;

-- -----------------------------------------------------------------------------
--       TABLE : EVENEMENT
-- -----------------------------------------------------------------------------
CREATE SEQUENCE evenement_seq
START WITH
    1 INCREMENT BY 1 NOCACHE NOCYCLE;

-- -----------------------------------------------------------------------------
--       TABLE : PERIODICITE
-- -----------------------------------------------------------------------------
CREATE SEQUENCE periodicite_seq
START WITH
    1 INCREMENT BY 1 NOCACHE NOCYCLE;

-- -----------------------------------------------------------------------------
--       TABLE : JOUR
-- -----------------------------------------------------------------------------
CREATE SEQUENCE jour_seq
START WITH
    1 INCREMENT BY 1 NOCACHE NOCYCLE;

-- -----------------------------------------------------------------------------
--       TABLE : OCCURRENCE
-- -----------------------------------------------------------------------------
CREATE SEQUENCE occurrence_seq
START WITH
    1 INCREMENT BY 1 NOCACHE NOCYCLE;

-- -----------------------------------------------------------------------------
--       TABLE : LOCALISATION
-- -----------------------------------------------------------------------------
CREATE SEQUENCE localisation_seq
START WITH
    1 INCREMENT BY 1 NOCACHE NOCYCLE;

exit;
