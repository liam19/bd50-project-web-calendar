create or replace
PACKAGE PA_ROLE_UTILISATEUR
AS
    -- Ref Cursor
    TYPE c_type_role IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numrole IN NUMBER, in_nomrole IN VARCHAR2);

    -- Get
    FUNCTION Get(in_numrole IN NUMBER) RETURN role_utilisateur%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_role;

    -- Update
    PROCEDURE Upd(in_numrole IN NUMBER, in_nomrole IN VARCHAR2);

    -- Delete
    PROCEDURE Del(in_numrole IN NUMBER);
END;
/

exit;
