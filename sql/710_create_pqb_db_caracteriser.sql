CREATE OR REPLACE PACKAGE BODY PA_CARACTERISER
AS
    -- Insert
    PROCEDURE Add(in_numtheme IN NUMBER, in_numevt IN NUMBER)
    IS
    BEGIN
        INSERT INTO
            caracteriser
        VALUES
            (in_numtheme, in_numevt);

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
    -- FUNCTION Get(in_adrmail IN VARCHAR2) RETURN caracteriser%ROWTYPE;

    -- GetAll
    FUNCTION GetAll
    RETURN c_type_caract
    IS
        c_caract c_type_caract;
    BEGIN
        -- select all data from caracteriser
        OPEN c_caract FOR SELECT * FROM caracteriser;
        RETURN c_caract;
    END;

    -- Delete
    PROCEDURE Del(in_numtheme IN NUMBER, in_numevt IN NUMBER)
    IS
    BEGIN
        DELETE FROM caracteriser
            WHERE numtheme = in_numtheme AND numevt = in_numevt;

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
END;
/

exit;
