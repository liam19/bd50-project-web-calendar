create or replace
PACKAGE BODY PA_ADRESSE_MAIL
AS
    -- Insert
    PROCEDURE Add(in_adrmail IN VARCHAR2, in_numuser IN NUMBER, in_statmail IN VARCHAR2)
    IS
    BEGIN
        INSERT INTO
            adresse_mail
        VALUES
            (in_adrmail, in_numuser, in_statmail);

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
    FUNCTION Get(in_adrmail IN VARCHAR2) 
    RETURN adresse_mail%ROWTYPE
    IS
        l_mail adresse_mail%ROWTYPE;
    BEGIN
        SELECT ADR.* INTO l_mail FROM adresse_mail ADR WHERE ADR.adrmail = in_adrmail;
        RETURN l_mail;
    END;

    -- GetAll
    FUNCTION GetAll
    RETURN c_type_adrmail
    IS
        c_adrmail c_type_adrmail;
    BEGIN
        -- select all data from adresse_mail
        OPEN c_adrmail FOR SELECT * FROM adresse_mail;
        RETURN c_adrmail;
    END;

    -- Update
    PROCEDURE Upd(in_adrmail IN VARCHAR2, in_numuser IN NUMBER, in_statmail IN VARCHAR2)    
    IS
    BEGIN
        UPDATE adresse_mail
        SET numuser = in_numuser, statmail = in_statmail
        WHERE adrmail = in_adrmail;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    PROCEDURE UpdNumUser(in_adrmail IN VARCHAR2, in_numuser IN NUMBER)
    IS
    BEGIN
        UPDATE adresse_mail
            SET numuser = in_numuser
            WHERE adrmail = in_adrmail;

            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Delete
    PROCEDURE Del(in_adrmail IN VARCHAR2)
    IS
    BEGIN
        DELETE FROM adresse_mail
        WHERE adrmail = in_adrmail;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Exists
    FUNCTION Exist(in_adrmail IN VARCHAR2)
    RETURN BOOLEAN
    IS
        result INTEGER;
    BEGIN
        SELECT COUNT(*) INTO result FROM adresse_mail WHERE adrmail = in_adrmail;
        IF result > 0 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;

    -- Link
    PROCEDURE LinkToUser(in_adrmail IN VARCHAR2)
    IS
        numuser utilisateur.numuser%TYPE;
    BEGIN
        -- get the user ID
        SELECT UTL.numuser INTO numuser FROM utilisateur UTL WHERE UTL.adrmail = in_adrmail;
        -- link mail and user
        UpdNumUser(in_adrmail, numuser);
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
END;
/

exit;
