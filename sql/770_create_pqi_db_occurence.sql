create or replace
PACKAGE PA_OCCURRENCE
AS
    -- Ref Cursor
    TYPE c_type_occurrence IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numevt IN NUMBER, in_numloc IN NUMBER, in_debutoccur IN DATE, in_finoccur IN DATE, 
	in_infoccur IN VARCHAR2, in_statoccur IN VARCHAR2);

    -- Get
    /*FUNCTION Get(in_adrmail IN VARCHAR2) RETURN adresse_mail%ROWTYPE;*/

    -- GetAll
    FUNCTION GetAll RETURN c_type_occurrence;

    -- Update
    PROCEDURE Upd(in_numevt IN NUMBER, in_numoccur IN NUMBER, in_numloc IN NUMBER, 
	in_debutoccur IN DATE, in_finoccur IN DATE, in_infoccur IN VARCHAR2, in_statoccur IN VARCHAR2);

    -- Delete
    PROCEDURE Del(in_numevt IN NUMBER, in_numoccur IN NUMBER);

    -- Exists
    FUNCTION Exist(in_numevt IN NUMBER, in_numoccur IN NUMBER) RETURN BOOLEAN;

    -- Get Number of Occurrences by Event
    FUNCTION CountOccurrencesByEvent(in_numevent IN NUMBER) RETURN NUMBER;

    -- Get Event Number given an Occurrence Number
    FUNCTION GetEventNumberByOccurrence(in_numoccur IN NUMBER) RETURN NUMBER;
END;
/

exit;
