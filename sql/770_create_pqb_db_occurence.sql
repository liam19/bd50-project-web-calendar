create or replace
PACKAGE BODY PA_OCCURRENCE
AS
    -- Insert
    PROCEDURE Add(in_numevt IN NUMBER, in_numloc IN NUMBER, in_debutoccur IN DATE, in_finoccur IN DATE, in_infoccur IN VARCHAR2, in_statoccur IN VARCHAR2)
    IS
    BEGIN
        INSERT INTO
            occurrence (NUMEVT, NUMLOC, DEBUTOCCUR, FINOCCUR, INFOCCUR, STATOCCUR)
        VALUES
            (in_numevt, in_numloc, in_debutoccur, in_finoccur, in_infoccur, in_statoccur);

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
   /* FUNCTION Get(in_numuser IN NUMBER, in_numevt IN NUMBER, in_numoccur IN NUMBER) 
    RETURN partager%ROWTYPE
    IS
        l_mail partager%ROWTYPE;
    BEGIN
        SELECT ADR.* INTO l_mail FROM adresse_mail ADR WHERE ADR.adrmail = in_adrmail;
        RETURN l_mail;
    END;*/

    -- GetAll
    FUNCTION GetAll
    RETURN c_type_occurrence
    IS
        c_occurrence c_type_occurrence;
    BEGIN
        -- select all data from occurrence
        OPEN c_occurrence FOR SELECT * FROM occurrence;
        RETURN c_occurrence;
    END;

    -- Update
    PROCEDURE Upd(in_numevt IN NUMBER, in_numoccur IN NUMBER, in_numloc IN NUMBER, 
	in_debutoccur IN DATE, in_finoccur IN DATE, in_infoccur IN VARCHAR2, in_statoccur IN VARCHAR2)    
    IS
    BEGIN
        UPDATE occurrence
        SET numloc = in_numloc , debutoccur = in_debutoccur, finoccur = in_finoccur, infoccur = in_infoccur, statoccur = in_statoccur
        WHERE numevt = in_numevt AND numoccur = in_numoccur;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Delete
    PROCEDURE Del(in_numevt IN NUMBER, in_numoccur IN NUMBER)
    IS
    BEGIN
        DELETE FROM occurrence
        WHERE numevt = in_numevt AND numoccur = in_numoccur;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Exists
    FUNCTION Exist(in_numevt IN NUMBER, in_numoccur IN NUMBER)
    RETURN BOOLEAN
    IS
        result INTEGER;
    BEGIN
        SELECT COUNT(*) INTO result FROM occurrence 
		WHERE numevt = in_numevt AND numoccur = in_numoccur;
        IF result > 0 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;

    FUNCTION CountOccurrencesByEvent(in_numevent IN NUMBER)
    RETURN NUMBER
    IS
        result NUMBER;
    BEGIN
        SELECT COUNT(*) 
        INTO result 
        FROM occurrence 
        WHERE numevt = in_numevent;

        RETURN result;
    END;

    -- Get Event Number given an Occurrence Number
    FUNCTION GetEventNumberByOccurrence(in_numoccur IN NUMBER) 
    RETURN NUMBER
    IS
        result NUMBER;
    BEGIN
        SELECT
            numevt INTO result
        FROM
            occurrence
        WHERE
            numoccur = in_numoccur;

        RETURN result;
    END;


END;
/

exit;
