create or replace
PACKAGE UI_USER
AS
    PROCEDURE ShowUserAccount;
    
    PROCEDURE FormEditUserAccount;

    PROCEDURE ExecEditUserAccount (
        vnom IN VARCHAR2, vprenom IN VARCHAR2, vtel IN VARCHAR2, 
        vmail IN VARCHAR2, vpassword IN VARCHAR2 DEFAULT NULL
    );
END;
/

exit;
