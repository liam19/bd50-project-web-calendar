set define off
set verify off
set feedback off
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
begin wwv_flow.g_import_in_progress := true; end;
/
 
--       AAAA       PPPPP   EEEEEE  XX      XX
--      AA  AA      PP  PP  EE       XX    XX
--     AA    AA     PP  PP  EE        XX  XX
--    AAAAAAAAAA    PPPPP   EEEE       XXXX
--   AA        AA   PP      EE        XX  XX
--  AA          AA  PP      EE       XX    XX
--  AA          AA  PP      EEEEEE  XX      XX
begin
select value into wwv_flow_api.g_nls_numeric_chars from nls_session_parameters where parameter='NLS_NUMERIC_CHARACTERS';
execute immediate 'alter session set nls_numeric_characters=''.,''';
end;
/
prompt  WORKSPACE 2096823335338117
--
-- Workspace, User Group, User, and Team Development Export:
--   Date and Time:   11:35 Saturday June 20, 2020
--   Exported By:     G06_WEBCAL
--   Export Type:     Workspace Export
--   Version:         4.2.1.00.08
--   Instance ID:     69303564171094
--
-- Import:
--   Using Instance Administration / Manage Workspaces
--   or
--   Using SQL*Plus as the Oracle user APEX_040200
 
begin
    wwv_flow_api.set_security_group_id(p_security_group_id=>2096823335338117);
end;
/
----------------
-- W O R K S P A C E
-- Creating a workspace will not create database schemas or objects.
-- This API creates only the meta data for this APEX workspace
prompt  Creating workspace G06_WEBCAL...
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
end;
/
begin
wwv_flow_fnd_user_api.create_company (
  p_id => 2096902344338173
 ,p_provisioning_company_id => 2096823335338117
 ,p_short_name => 'G06_WEBCAL'
 ,p_display_name => 'G06_WEBCAL'
 ,p_first_schema_provisioned => 'G06_WEBCAL'
 ,p_company_schemas => 'G06_WEBCAL'
 ,p_account_status => 'ASSIGNED'
 ,p_allow_plsql_editing => 'Y'
 ,p_allow_app_building_yn => 'Y'
 ,p_allow_sql_workshop_yn => 'Y'
 ,p_allow_websheet_dev_yn => 'Y'
 ,p_allow_team_development_yn => 'Y'
 ,p_allow_to_be_purged_yn => 'Y'
 ,p_allow_restful_services_yn => 'Y'
 ,p_source_identifier => 'G06_WEBC'
 ,p_path_prefix => 'G06_WEBCAL'
 ,p_workspace_image => wwv_flow_api.g_varchar2_table
);
end;
/
----------------
-- G R O U P S
--
prompt  Creating Groups...
----------------
-- U S E R S
-- User repository for use with APEX cookie-based authentication.
--
prompt  Creating Users...
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id      => '2096712412338117',
  p_user_name    => 'G06_WEBCAL',
  p_first_name   => 'admin',
  p_last_name    => 'admin',
  p_description  => '',
  p_email_address=> 'admin@utbm.fr',
  p_web_password => 'FD91591B5E83ACBC0B0E8B55D844D795',
  p_web_password_format => 'HEX_ENCODED_DIGEST_V2',
  p_group_ids                    => '',
  p_developer_privs              => 'ADMIN:CREATE:DATA_LOADER:EDIT:HELP:MONITOR:SQL',
  p_default_schema               => 'G06_WEBCAL',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('202006201117','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'Y',
  p_first_password_use_occurred  => 'Y',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
----------------
--Application Builder Preferences
--
----------------
--Click Count Logs
--
----------------
--csv data loading
--
----------------
--mail
--
----------------
--mail log
--
----------------
--app models
--
----------------
--password history
--
begin
  wwv_flow_api.create_password_history (
    p_id => 2097209688338190,
    p_user_id => 2096712412338117,
    p_password => 'C0B4386CA0DEB87FCFD7FE6D0846A747');
end;
/
begin
  wwv_flow_api.create_password_history (
    p_id => 2098827660369662,
    p_user_id => 2096712412338117,
    p_password => 'A1E21EFC4711A61CB34A40620F19BC13');
end;
/
----------------
--package app map
--
----------------
--preferences
--
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2099105702371540,
    p_user_id => 'G06_WEBCAL',
    p_preference_name => 'FSP_IR_4000_P1500_W3519715528105919',
    p_attribute_value => '3521529006112497____3521529006112497');
end;
/
----------------
--service mods
--
----------------
--query builder
--
----------------
--sql scripts
--
----------------
--sql commands
--
----------------
--user access log
--
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'G06_WEBCAL',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('202006201115','YYYYMMDDHH24MI'),
    p_ip_address => '10.0.2.15',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 5,
    p_custom_status_text => 'Invalid Login Credentials');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'G06_WEBCAL',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('202006201116','YYYYMMDDHH24MI'),
    p_ip_address => '10.0.2.15',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 5,
    p_custom_status_text => 'Invalid Login Credentials');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'G06_WEBCAL',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('202006201118','YYYYMMDDHH24MI'),
    p_ip_address => '10.0.2.15',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
prompt Check Compatibility...
begin
-- This date identifies the minimum version required to import this file.
wwv_flow_team_api.check_version(p_version_yyyy_mm_dd=>'2010.05.13');
end;
/
 
begin wwv_flow.g_import_in_progress := true; wwv_flow.g_user := USER; end; 
/
 
--
prompt ...news
--
begin
null;
end;
/
--
prompt ...links
--
begin
null;
end;
/
--
prompt ...bugs
--
begin
null;
end;
/
--
prompt ...events
--
begin
null;
end;
/
--
prompt ...features
--
begin
null;
end;
/
--
prompt ...tasks
--
begin
null;
end;
/
--
prompt ...feedback
--
begin
null;
end;
/
--
prompt ...task defaults
--
begin
null;
end;
/
commit;
begin
execute immediate 'begin sys.dbms_session.set_nls( param => ''NLS_NUMERIC_CHARACTERS'', value => '''''''' || replace(wwv_flow_api.g_nls_numeric_chars,'''''''','''''''''''') || ''''''''); end;';
end;
/
set verify on
set feedback on
set define on
prompt  ...done
