create or replace
PACKAGE BODY PA_UTILISATEUR
AS
    -- Insert
    PROCEDURE Add(
        in_adrmail IN VARCHAR2, in_numrole IN NUMBER, in_pseudouser IN VARCHAR2,
        in_mdpuser IN VARCHAR2, in_nomuser IN VARCHAR2, in_prenomuser IN VARCHAR2,
        in_statutuser IN VARCHAR2, in_teluser IN VARCHAR2
    ) IS
    BEGIN
        INSERT INTO
            utilisateur 
            (
                adrmail, numrole, pseudouser, 
                mdpuser, nomuser, prenomuser, 
                statutuser, teluser
            )
        VALUES
            (
                in_adrmail, in_numrole, in_pseudouser,
                in_mdpuser, in_nomuser, in_prenomuser,
                in_statutuser, in_teluser
            );

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            raise_application_error(-20001, 'Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
    FUNCTION Get(in_numuser IN VARCHAR2) 
    RETURN utilisateur%ROWTYPE
    IS
        l_utilisateur utilisateur%ROWTYPE;
    BEGIN
        SELECT UTL.* INTO l_utilisateur FROM utilisateur UTL WHERE UTL.numuser = in_numuser;
        RETURN l_utilisateur;
    END;

    -- Get by pseudo
    FUNCTION GetByPseudo(in_pseudo IN VARCHAR2)
    RETURN utilisateur%ROWTYPE
    IS
        l_utilisateur utilisateur%ROWTYPE;
    BEGIN
        SELECT UTL.* INTO l_utilisateur FROM utilisateur UTL WHERE UTL.pseudouser = in_pseudo;
        RETURN l_utilisateur;
    END;

    -- Get by adrmail
    FUNCTION GetByAdrmail(in_adrmail IN VARCHAR2)
    RETURN utilisateur%ROWTYPE
    IS
        l_utilisateur utilisateur%ROWTYPE;
    BEGIN
        SELECT UTL.* INTO l_utilisateur FROM utilisateur UTL WHERE UTL.adrmail = in_adrmail;
        RETURN l_utilisateur;
    END;

    -- GetAll
    FUNCTION GetAll 
    RETURN c_type_utilisateur
    IS
        c_utilisateur c_type_utilisateur;
    BEGIN
        -- select all data from adresse_mail
        OPEN c_utilisateur FOR SELECT * FROM utilisateur;
        RETURN c_utilisateur;
    END;

    -- Update
    PROCEDURE Upd(
        in_numuser IN VARCHAR2, in_adrmail IN VARCHAR2, in_nomuser IN VARCHAR2, 
        in_prenomuser IN VARCHAR2, in_teluser IN VARCHAR2
    ) IS
    BEGIN
        UPDATE utilisateur
        SET 
            adrmail = in_adrmail, nomuser = in_nomuser, prenomuser = in_prenomuser,
            teluser = in_teluser
        WHERE numuser = in_numuser;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            raise_application_error(-20001, 'Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Delete
    PROCEDURE Del(in_numuser IN VARCHAR2)
    IS
    BEGIN
        DELETE FROM utilisateur
        WHERE numuser = in_numuser;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            raise_application_error(-20001, 'Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
END;
/

exit;
