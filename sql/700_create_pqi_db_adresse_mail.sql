create or replace
PACKAGE PA_ADRESSE_MAIL
AS
    -- Ref Cursor
    TYPE c_type_adrmail IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_adrmail IN VARCHAR2, in_numuser IN NUMBER, in_statmail IN VARCHAR2);

    -- Get
    FUNCTION Get(in_adrmail IN VARCHAR2) RETURN adresse_mail%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_adrmail;

    -- Update
    PROCEDURE Upd(in_adrmail IN VARCHAR2, in_numuser IN NUMBER, in_statmail IN VARCHAR2);

    -- Update NumUser
    PROCEDURE UpdNumUser(in_adrmail IN VARCHAR2, in_numuser IN NUMBER);

    -- Delete
    PROCEDURE Del(in_adrmail IN VARCHAR2);

    -- Exists
    FUNCTION Exist(in_adrmail IN VARCHAR2) RETURN BOOLEAN;

    -- Link
    PROCEDURE LinkToUser(in_adrmail IN VARCHAR2);
END;
/

exit;
