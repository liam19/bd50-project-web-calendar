CREATE OR REPLACE PACKAGE PA_CONCERNER
AS
    -- Ref Cursor
    TYPE c_type_concerner IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numperiod IN NUMBER, in_idjour IN NUMBER);

    -- Get
    -- FUNCTION Get(in_adrmail IN VARCHAR2) RETURN caracteriser%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_concerner;

    -- Delete
    PROCEDURE Del(in_numperiod IN NUMBER, in_idjour IN NUMBER);
	
	--EXISTS
	FUNCTION Exist(in_numperiod IN NUMBER, in_idjour IN NUMBER) RETURN BOOLEAN;
END;
/

exit;
