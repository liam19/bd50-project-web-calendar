create or replace
PACKAGE PA_JOUR
AS
    -- Ref Cursor
    TYPE c_type_jour IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_nomjour IN VARCHAR2);

    -- Get
    FUNCTION Get(in_idjour IN NUMBER) RETURN jour%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_jour;

    -- Update
    PROCEDURE Upd(in_idjour IN NUMBER, in_nomjour IN VARCHAR2);

    -- Delete
    PROCEDURE Del(in_idjour IN NUMBER);
END;
/

exit;
