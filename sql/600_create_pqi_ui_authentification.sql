create or replace
PACKAGE UI_AUTH
AS
    -- User Authentification Page
    PROCEDURE FormConnectUser;
    -- User Account Creation Page
    PROCEDURE FormCreateUserAccount;

    -- Create User Account
    PROCEDURE ExecCreateUserAccount(
        vpseudo IN VARCHAR2, vnom IN VARCHAR2, vprenom IN VARCHAR2,
        vtel IN VARCHAR2, vmail IN VARCHAR2, vpassword IN VARCHAR2
    );
    -- Connect User
    PROCEDURE ExecConnectUser(vpseudo IN VARCHAR2, vpassword IN VARCHAR2);

END;
/

exit;
