CREATE OR REPLACE PACKAGE BODY PA_CONCERNER
AS
    -- Insert
    PROCEDURE Add(in_numperiod IN NUMBER, in_idjour IN NUMBER)
    IS
    BEGIN
        INSERT INTO
            concerner
        VALUES
            (in_numperiod, in_idjour);

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
    -- FUNCTION Get(in_adrmail IN VARCHAR2) RETURN caracteriser%ROWTYPE;

    -- GetAll
    FUNCTION GetAll
    RETURN c_type_concerner
    IS
        c_concerner c_type_concerner;
    BEGIN
        -- select all data from caracteriser
        OPEN c_concerner FOR SELECT * FROM concerner;
        RETURN c_concerner;
    END;

    -- Delete
    PROCEDURE Del(in_numperiod IN NUMBER, in_idjour IN NUMBER)
    IS
    BEGIN
        DELETE FROM concerner
            WHERE numperiod = in_numperiod AND idjour = in_idjour;

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
	
	-- Exists
    FUNCTION Exist(in_numperiod IN NUMBER, in_idjour IN NUMBER)
    RETURN BOOLEAN
    IS
        result INTEGER;
    BEGIN
        SELECT COUNT(*) INTO result FROM concerner 
		WHERE numperiod = in_numperiod AND idjour = in_idjour;
        IF result > 0 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;
END;
/

exit;
