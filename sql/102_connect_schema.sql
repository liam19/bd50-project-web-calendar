CREATE OR REPLACE PROCEDURE home IS
BEGIN
    HTP.htmlOpen;
        HTP.headOpen;
            HTP.title('Page d''accueil');
            HTP.print('<link href="/public/G06_WEBCAL/css/style.css" rel="stylesheet" type="text/css" />');
        HTP.headClose;
        HTP.bodyOpen;
            HTP.print('<DIV id="home">');
                HTP.tableOpen(cattributes => 'id="central-card"');
                    HTP.tableHeader('');
                    HTP.tableRowOpen;
                        HTP.formOpen(owa_util.get_owa_service_path || 'ui_auth.formconnectuser', 'POST');
                            HTP.tableData(HTF.formSubmit(cvalue => 'Se connecter à  un compte existant ?'), cattributes => 'class="no-border"');
                        HTP.formClose;
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData(HTF.anchor(owa_util.get_owa_service_path || 'ui_auth.formcreateuseraccount', 'Aucun compte ? Inscrivez-vous ici !'), cattributes => 'class="no-border"');
                    HTP.tableRowClose;

                HTP.tableClose;
            HTP.print('</DIV>');
        HTP.bodyClose;
    HTP.htmlClose;
END home;
/

BEGIN
    DBMS_EPG.create_dad(
        dad_name => 'webcal_dad',
        path => '/webcalendar/*'
    );
END;
/
BEGIN
    DBMS_EPG.set_dad_attribute (
        dad_name => 'webcal_dad',
        attr_name => 'default-page',
        attr_value => 'home'
    );
        DBMS_EPG.set_dad_attribute (
        dad_name => 'webcal_dad',
        attr_name => 'authentication-mode',
        attr_value => 'Basic'
    );
        DBMS_EPG.set_dad_attribute (
        dad_name => 'webcal_dad',
        attr_name => 'database-username',
        attr_value => 'G06_WEBCAL'
    );
END;
/

exit;
