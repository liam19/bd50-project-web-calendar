create or replace
PACKAGE UI_CONTACT
AS
    -- Contact List Page
    PROCEDURE ContactList;

    -- Add Contact Page
    PROCEDURE FormAddContact;

    -- Add Contact
    PROCEDURE ExecAddContact(videntification IN VARCHAR2);

    -- Remove Contact
    PROCEDURE ExecDeleteContact(in_numcontact IN NUMBER);
END;
/

exit;
