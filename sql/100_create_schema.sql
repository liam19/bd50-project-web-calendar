create user G06_WEBCAL identified by WEBCAL default tablespace BD50_DATA temporary tablespace TEMP;

alter system set local_listener = "(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))" scope=both ;

exec dbms_xdb.setftpport(2100);

exec dbms_xdb.sethttpport(8080);

alter system register;

alter user xdb identified by manager account unlock ;

exit ;
