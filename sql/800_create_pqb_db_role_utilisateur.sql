create or replace
PACKAGE BODY PA_ROLE_UTILISATEUR
AS
    -- Insert
    PROCEDURE Add(in_numrole IN NUMBER, in_nomrole IN VARCHAR2)
    IS
    BEGIN
        INSERT INTO
            role_utilisateur
        VALUES
            (in_numrole, in_nomrole);

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Get
    FUNCTION Get(in_numrole IN NUMBER) 
    RETURN role_utilisateur%ROWTYPE
    IS
        l_role role_utilisateur%ROWTYPE;
    BEGIN
        SELECT ROL.* INTO l_role FROM role_utilisateur ROL WHERE ROL.numrole = in_numrole;
        RETURN l_role;
    END;

    -- GetAll
    FUNCTION GetAll 
    RETURN c_type_role
    IS
        c_role c_type_role;
    BEGIN
        -- select all data from adresse_mail
        OPEN c_role FOR SELECT * FROM role_utilisateur;
        RETURN c_role;
    END;

    -- Update
    PROCEDURE Upd(in_numrole IN NUMBER, in_nomrole IN VARCHAR2)
    IS
    BEGIN
        UPDATE role_utilisateur
        SET nomrole = in_nomrole
        WHERE numrole = in_numrole;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Delete
    PROCEDURE Del(in_numrole IN NUMBER)
    IS
    BEGIN
        DELETE FROM role_utilisateur
        WHERE numrole = in_numrole;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
END;
/

exit;
