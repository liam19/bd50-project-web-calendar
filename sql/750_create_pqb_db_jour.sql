create or replace
PACKAGE BODY PA_JOUR
AS
    -- Insert
    PROCEDURE Add(in_nomjour IN VARCHAR2)
    IS
    BEGIN
        INSERT INTO
            jour (nomjour)
        VALUES
            (in_nomjour);

            COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;


    -- Get
    FUNCTION Get(in_idjour IN NUMBER) RETURN jour%ROWTYPE
    IS
        l_jour jour%ROWTYPE;
    BEGIN
        SELECT JOR.* INTO l_jour FROM jour JOR WHERE JOR.idjour = in_idjour;
        RETURN l_jour;
    END;

    -- GetAll
    FUNCTION GetAll RETURN c_type_jour
    IS
        c_jour c_type_jour;
    BEGIN
        -- select all data from jour
        OPEN c_jour FOR SELECT * FROM jour;
        RETURN c_jour;
    END;


    -- Update
    PROCEDURE Upd(in_idjour IN NUMBER, in_nomjour IN VARCHAR2)
    IS
    BEGIN
        UPDATE jour
        SET nomjour = in_nomjour
        WHERE idjour = in_idjour;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Delete
    PROCEDURE Del(in_idjour IN NUMBER)
    IS
    BEGIN
        DELETE FROM jour
        WHERE idjour = in_idjour;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
END;
/

exit;
