create or replace
PACKAGE BODY PA_EVENEMENT
AS
    -- Insert
    PROCEDURE Add(
        in_numperiod IN NUMBER, in_numuser IN NUMBER,
		in_titreevt IN VARCHAR2, in_dispoevt IN NUMBER, in_descrevt IN VARCHAR2
    ) IS
    BEGIN
        INSERT INTO
            evenement 
            (
                numperiod, numuser, 
                titreevt, dispoevt, descrevt
            )
        VALUES
            (
                in_numperiod, in_numuser,
                in_titreevt, in_dispoevt, in_descrevt
            );

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when inserting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    /*-- Get
    FUNCTION Get(in_numuser IN VARCHAR2) 
    RETURN utilisateur%ROWTYPE
    IS
        l_utilisateur utilisateur%ROWTYPE;
    BEGIN
        SELECT UTL.* INTO l_utilisateur FROM utilisateur UTL WHERE UTL.numuser = in_numuser;
        RETURN l_utilisateur;
    END;

    -- Get by pseudo
    FUNCTION GetByPseudo(in_pseudo IN VARCHAR2)
    RETURN utilisateur%ROWTYPE
    IS
        l_utilisateur utilisateur%ROWTYPE;
    BEGIN
        SELECT UTL.* INTO l_utilisateur FROM utilisateur UTL WHERE UTL.pseudouser = in_pseudo;
        RETURN l_utilisateur;
    END;*/

    -- GetAll
    FUNCTION GetAll 
    RETURN c_type_evenement
    IS
        c_evenement c_type_evenement;
    BEGIN
        -- select all data from evenement
        OPEN c_evenement FOR SELECT * FROM evenement;
        RETURN c_evenement;
    END;

    -- Update
    PROCEDURE Upd(
        in_numevt IN NUMBER, in_numperiod IN NUMBER, in_numuser IN NUMBER,
		in_titreevt IN VARCHAR2, in_dispoevt IN NUMBER, in_descrevt IN VARCHAR2
    ) IS
    BEGIN
        UPDATE evenement
        SET 
            numperiod = in_numperiod, numuser = in_numuser, titreevt = in_titreevt,
			dispoevt = in_dispoevt, descrevt = in_descrevt
        WHERE numevt = in_numevt;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when updating data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;

    -- Delete
    PROCEDURE Del(in_numevt IN NUMBER)
    IS
    BEGIN
        DELETE FROM evenement
        WHERE numevt = in_numevt;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Exception occurred when deleting data : ' || to_char(SQLCODE) || ' | ' || SQLERRM);
    END;
	
	-- Exists
    FUNCTION Exist(in_numevt IN NUMBER)
    RETURN BOOLEAN
    IS
        result INTEGER;
    BEGIN
        SELECT COUNT(*) INTO result FROM evenement 
		WHERE numevt = in_numevt;
        IF result > 0 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;

END;
/

exit;
