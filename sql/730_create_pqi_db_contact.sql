CREATE OR REPLACE PACKAGE PA_CONTACT
AS
    -- Ref Cursor
    TYPE c_type_contact IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numuser IN NUMBER, in_numcontact IN NUMBER);

    -- Get
    FUNCTION Get(in_numuser IN NUMBER, in_numcontact IN NUMBER) RETURN contact%ROWTYPE;

    -- Get All Contacts
    FUNCTION GetAllContacts(in_numuser IN NUMBER) RETURN c_type_contact;

    -- GetAll
    FUNCTION GetAll RETURN c_type_contact;

    -- Delete
    PROCEDURE Del(in_numuser IN NUMBER, in_numcontact IN NUMBER);
	
	--EXISTS
	FUNCTION Exist(in_numuser IN NUMBER, in_numcontact IN NUMBER) RETURN BOOLEAN;
END;
/

exit;
