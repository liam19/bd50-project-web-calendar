-- -----------------------------------------------------------------------------
--       TABLE : UTILISATEUR
-- -----------------------------------------------------------------------------
CREATE
OR REPLACE TRIGGER TBI_UTILISATEUR BEFORE
INSERT
    ON UTILISATEUR FOR EACH ROW BEGIN
        IF :new.NUMUSER IS NULL THEN
            SELECT
                utilisateur_seq.nextval INTO :new.NUMUSER
            FROM
                dual;
        END IF;
END;
/

-- -----------------------------------------------------------------------------
--       TABLE : THEME
-- -----------------------------------------------------------------------------
CREATE
OR REPLACE TRIGGER TBI_THEME BEFORE
INSERT
    ON THEME FOR EACH ROW BEGIN
        IF :new.NUMTHEME IS NULL THEN
        SELECT
            theme_seq.nextval INTO :new.NUMTHEME
        FROM
            dual;

END IF;

END;
/
-- -----------------------------------------------------------------------------
--       TABLE : EVENEMENT
-- -----------------------------------------------------------------------------
CREATE
OR REPLACE TRIGGER TBI_EVENEMENT BEFORE
INSERT
    ON EVENEMENT FOR EACH ROW BEGIN
        IF :new.NUMEVT IS NULL THEN
        SELECT
            evenement_seq.nextval INTO :new.NUMEVT
        FROM
            dual;

END IF;

END;
/
-- -----------------------------------------------------------------------------
--       TABLE : PERIODICITE
-- -----------------------------------------------------------------------------
CREATE
OR REPLACE TRIGGER TBI_PERIODICITE BEFORE
INSERT
    ON PERIODICITE FOR EACH ROW BEGIN
        IF :new.NUMPERIOD IS NULL THEN
        SELECT
            periodicite_seq.nextval INTO :new.NUMPERIOD
        FROM
            dual;

END IF;

END;
/
-- -----------------------------------------------------------------------------
--       TABLE : JOUR
-- -----------------------------------------------------------------------------
CREATE
OR REPLACE TRIGGER TBI_JOUR BEFORE
INSERT
    ON JOUR FOR EACH ROW BEGIN
        IF :new.IDJOUR IS NULL THEN
        SELECT
            jour_seq.nextval INTO :new.IDJOUR
        FROM
            dual;

END IF;

END;
/
-- -----------------------------------------------------------------------------
--       TABLE : OCCURRENCE
-- -----------------------------------------------------------------------------
CREATE
OR REPLACE TRIGGER TBI_OCCURRENCE BEFORE
INSERT
    ON OCCURRENCE FOR EACH ROW BEGIN
        IF :new.NUMOCCUR IS NULL THEN
        SELECT
            occurrence_seq.nextval INTO :new.NUMOCCUR
        FROM
            dual;

END IF;

END;
/
-- -----------------------------------------------------------------------------
--       TABLE : LOCALISATION
-- -----------------------------------------------------------------------------
CREATE
OR REPLACE TRIGGER TBI_LOCALISATION BEFORE
INSERT
    ON LOCALISATION FOR EACH ROW BEGIN
        IF :new.NUMLOC IS NULL THEN
        SELECT
            localisation_seq.nextval INTO :new.NUMLOC
        FROM
            dual;

END IF;

END;
/

exit;
