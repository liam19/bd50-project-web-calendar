create or replace
PACKAGE BODY UI_CALENDAR
AS
    -- Event List Page
    PROCEDURE EventList
    IS
        CURSOR c_occurrence (in_numusr utilisateur%ROWTYPE) IS 
          SELECT
            OCC.debutoccur as debutoccur,
            OCC.finoccur as finoccur,
            EVT.titreevt as titreevt,
            EVT.descrevt as descrevt,
            LOC.descrloc as descrloc,
            LOC.rueloc as rueloc,
            LOC.cploc as cploc,
            LOC.villeloc as villeloc,
            LOC.paysloc as paysloc,
            OCC.numoccur as numoccur
          FROM
            occurrence OCC
            INNER JOIN evenement EVT ON OCC.numevt = EVT.numevt
            INNER JOIN localisation LOC ON OCC.numloc = LOC.numloc
          WHERE
            numuser = in_numusr.numuser
          ORDER BY
            debutoccur ASC;
        
        usr utilisateur%ROWTYPE;
        occur c_occurrence%ROWTYPE;
    BEGIN
        -- get user by ID
        usr := pa_utils.get_user_connected();

        htp.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        htp.htmlOpen;
        htp.headOpen;
        htp.title( 'Calendrier');
        htp.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        htp.headClose;
        htp.bodyOpen;
           
        ui_common.header;
            HTP.header(1, 'Calendrier de l''utilisateur ' || usr.prenomuser || ' ' || usr.nomuser);
			      HTP.tableOpen;
              HTP.tableHeader('Début');
              HTP.tableHeader('Titre');
              HTP.tableHeader('Description');
              HTP.tableHeader('Localisation');
              HTP.tableHeader('Rue');
              HTP.tableHeader('Code Postal');
              HTP.tableHeader('Ville');
              HTP.tableHeader('Pays');
              HTP.formOpen(owa_util.get_owa_service_path || 'ui_event.formcreateanevent', 'POST');
                HTP.tableHeader(HTF.formSubmit(cvalue => 'Créer un evenement'), cattributes => 'class="no-border"');
              HTP.formClose;
              OPEN c_occurrence(usr);  
              LOOP  
                  FETCH c_occurrence into occur;
                  EXIT WHEN c_occurrence%NOTFOUND;
                  HTP.tableRowOpen;
                      HTP.tableData(occur.debutoccur);  
                      HTP.tableData(occur.titreevt);
                      HTP.tableData(occur.descrevt);
                      HTP.tableData(occur.descrloc);
                      HTP.tableData(occur.rueloc);
                      HTP.tableData(occur.cploc);
                      HTP.tableData(occur.villeloc);
                      HTP.tableData(occur.paysloc);
                      HTP.formOpen(owa_util.get_owa_service_path || 'ui_event.execdeleteevent?in_numoccur=' || occur.numoccur, 'POST');
                        HTP.tableData(HTF.formSubmit(cvalue => 'Supprimer'), cattributes => 'class="no-border"');
                      HTP.formClose;
                  HTP.tableRowClose;
                  HTP.print('</br>');
              END LOOP;
              

        HTP.tableClose;

        ui_common.footer;

        HTP.bodyClose;
        HTP.htmlClose;

    EXCEPTION
        WHEN no_data_found THEN
            ui_common.redirect('ui_auth.formconnectuser');
    END;        
END;
/

exit;
