create or replace
PACKAGE BODY UI_AUTH
IS
    -- User Authentification Page
    PROCEDURE FormConnectUser
    IS
    BEGIN
        IF pa_utils.get_cookie('login') IS NOT NULL AND pa_utils.get_cookie('mdp') IS NOT NULL  THEN
            ui_common.redirect('ui_calendar.eventlist');
        END IF;

        HTP.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        HTP.htmlOpen;
        HTP.headOpen;
        HTP.title( 'Calendrier');
        HTP.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        HTP.headClose;
        HTP.bodyOpen;
            HTP.header(1, 'Connexion');
            HTP.tableOpen;
                HTP.formOpen(owa_util.get_owa_service_path || 'ui_auth.execconnectuser', 'POST');

                    HTP.tableHeader('');
                    HTP.tableHeader('');
                    HTP.tableRowOpen;
                        HTP.tableData('Pseudonyme');
                        HTP.tableData(HTF.formText('vpseudo'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Mot de passe');
                        HTP.tableData(HTF.formPassword('vpassword'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('', cattributes => 'class="no-border"');
                        HTP.tableData(HTF.formSubmit(cvalue => 'Se connecter'), cattributes => 'class="no-border"');
                    HTP.tableRowClose;
                HTP.formClose;

                    HTP.tableRowOpen;
                        HTP.formOpen(owa_util.get_owa_service_path || 'home');
                            HTP.tableData('', cattributes => 'class="no-border"');
                            HTP.tableData(HTF.formSubmit(cvalue => 'Retour'), cattributes => 'class="no-border"');
                        HTP.formClose;
                    HTP.tableRowClose;
            HTP.tableClose;

            ui_common.footer;

        HTP.bodyClose;
        HTP.htmlClose;
    END;


    -- User Account Creation Page
    PROCEDURE FormCreateUserAccount
    IS
    BEGIN
        HTP.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        HTP.htmlOpen;
        HTP.headOpen;
        HTP.title( 'Calendrier');
        HTP.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        HTP.headClose;
        HTP.bodyOpen;
            HTP.title('Page de création de compte');
            -- HTP.print('<link href="public/G06_WEBCAL/css/calendar.css" rel="stylesheet" type="text/css" />');
        HTP.headClose;
        HTP.bodyOpen;
            HTP.header(1, 'Création d''un compte utilisateur');
            HTP.tableOpen;
                HTP.formOpen(owa_util.get_owa_service_path || 'ui_auth.execcreateuseraccount', 'POST');
                    HTP.tableHeader('');
                    HTP.tableHeader('');
                    HTP.tableRowOpen;
                        HTP.tableData('Pseudonyme');
                        HTP.tableData(HTF.formText('vpseudo'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Nom');
                        HTP.tableData(HTF.formText('vnom'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Prénom');
                        HTP.tableData(HTF.formText('vprenom'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Numéro de téléphone');
                        HTP.tableData(HTF.formText('vtel'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Adresse Mail');
                        HTP.tableData(HTF.formText('vmail'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Mot de passe');
                        HTP.tableData(HTF.formPassword('vpassword'));
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('', cattributes => 'class="no-border"');
                        HTP.tableData(HTF.formSubmit(cvalue => 'Créer'), cattributes => 'class="no-border"');
                    HTP.tableRowClose;

                HTP.formClose;

                HTP.tableRowOpen;
                    HTP.formOpen(owa_util.get_owa_service_path || 'home');
                        HTP.tableData('', cattributes => 'class="no-border"');
                        HTP.tableData(HTF.formSubmit(cvalue => 'Retour'), cattributes => 'class="no-border"');
                    HTP.formClose;
                HTP.tableRowClose;

            HTP.tableClose;

            ui_common.footer;
            
        HTP.bodyClose;
        HTP.htmlClose;
    END;

    -- Create User Account
    PROCEDURE ExecCreateUserAccount (
        vpseudo IN VARCHAR2, vnom IN VARCHAR2, vprenom IN VARCHAR2,
        vtel IN VARCHAR2, vmail IN VARCHAR2, vpassword IN VARCHAR2
    )
    IS
      pwdEncrypt RAW(32767);
    BEGIN
        -- if mail already exists, then redirect to FormCreateUserAccount
        IF  pa_adresse_mail.Exist(vmail) 
            OR NOT pa_utils.check_mail(vmail) 
            OR NOT pa_utils.check_tel(vtel)
        THEN
            ui_common.err_message('Erreur : Utilisateur non créé.', 'ui_auth.formcreateuseraccount');
        ELSE
             -- First, create the new mail
            pa_adresse_mail.Add(vmail, NULL, 'Attente');
            
            -- encrypt password
            pwdEncrypt := pa_utils.encrypt(vpassword);
            
            -- Then, create the user with the newly created mail
            -- num_role = 2 means USER
            pa_utilisateur.Add(vmail, 2, vpseudo, pwdEncrypt, vnom, vprenom, 'Attente', vtel);

            -- link the new mail with the new user
            pa_adresse_mail.LinkToUser(vmail);

            -- Redirect to the connection page
            ui_common.redirect('ui_auth.formconnectuser');
        END IF;
    EXCEPTION
        WHEN others THEN
            ui_common.err_message('Erreur : Utilisateur non créé.', 'ui_auth.formcreateuseraccount');
    END;
    
    -- Connect User
    PROCEDURE ExecConnectUser
      (vpseudo IN VARCHAR2, vpassword IN VARCHAR2)
    IS
      utl utilisateur%ROWTYPE;
    BEGIN
        -- get the right user
        utl := pa_utilisateur.GetByPseudo(vpseudo);
        -- if password equal, then connect user to its calendar
        IF pa_utils.Decrypt(utl.mdpuser) = vpassword THEN
            -- set the user session cookie
            pa_utils.set_cookie(utl.pseudouser, utl.mdpuser);
            -- redirect to the calendar
            ui_common.redirect('ui_calendar.eventlist');
        -- else, redirect
        ELSE
        ui_common.redirect('ui_auth.formconnectuser');
      END IF;
    EXCEPTION
    -- when the user in not found, redirect
      WHEN no_data_found THEN
        ui_common.redirect('ui_auth.formconnectuser');
    END;
END;
/

exit;
