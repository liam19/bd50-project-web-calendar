create or replace PACKAGE PA_UTILS 
AS

  -- encrypt a password
  FUNCTION encrypt (p_text  IN  VARCHAR2) RETURN RAW;
  
  -- decrypt a password
  FUNCTION decrypt (p_raw  IN  RAW) RETURN VARCHAR2;

  PROCEDURE set_langage_fr;
  
  -- get connect user via cookie
  FUNCTION get_user_connected RETURN utilisateur%rowtype;

  -- disconnect user
  PROCEDURE disconnect_user;

  -- remove cookie
  PROCEDURE remove_cookie;

  -- set a cookie
  PROCEDURE set_cookie(v_login IN VARCHAR2, v_mdp IN VARCHAR2);

  -- get cookie by name
  FUNCTION get_cookie(v_name IN VARCHAR2) RETURN VARCHAR2;

  -- check email adress with REGEX 
  FUNCTION check_mail(v_mail IN VARCHAR2) RETURN BOOLEAN;
  
  -- check phone number with REGEX
  FUNCTION check_tel(v_tel IN VARCHAR2) RETURN BOOLEAN;
  
END;
/

exit;
