create or replace
PACKAGE BODY UI_USER
AS
    PROCEDURE ShowUserAccount
    IS  
        usr utilisateur%ROWTYPE;
    BEGIN
        -- get connected user
        usr := pa_utils.get_user_connected();

        htp.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        htp.htmlOpen;
        htp.headOpen;
        htp.title( 'Compte');
        htp.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        htp.headClose;
        htp.bodyOpen;
           
        ui_common.header;
            HTP.header(1, 'Compte de l''utilisateur : ' || usr.prenomuser || ' ' || usr.nomuser);
                HTP.tableOpen;
                    HTP.tableHeader('');
                    HTP.tableHeader('');
                    HTP.tableRowOpen;
                        HTP.tableData('Pseudonyme');
                        HTP.tableData(usr.pseudouser);
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Nom');
                        HTP.tableData(usr.nomuser);
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Prénom');
                        HTP.tableData(usr.prenomuser);
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Numéro de téléphone');
                        HTP.tableData(usr.teluser);
                    HTP.tableRowClose;

                    HTP.tableRowOpen;
                        HTP.tableData('Adresse Mail');
                        HTP.tableData(usr.adrmail);
                    HTP.tableRowClose;
                    
                    HTP.tableRowOpen;
                        HTP.formOpen(owa_util.get_owa_service_path || 'ui_user.formedituseraccount', 'POST');
                            HTP.tableData(HTF.formSubmit(cvalue => 'Modifier mes informations'), cattributes => 'class="no-border"');
                            HTP.tableData('', cattributes => 'class="no-border"');
                        HTP.formClose;
                    HTP.tableRowClose;
                    
                HTP.tableClose;
                ui_common.footer;
        HTP.bodyClose;
        HTP.htmlClose;
    END;
    
    PROCEDURE FormEditUserAccount
    IS
        usr utilisateur%ROWTYPE;
    BEGIN
          -- get connected user
        usr := pa_utils.get_user_connected();

        htp.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        htp.htmlOpen;
        htp.headOpen;
        htp.title( 'Compte');
        htp.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        htp.headClose;
        htp.bodyOpen;

            ui_common.header;
            HTP.header(1, 'Compte de l''utilisateur : ' || usr.prenomuser || ' ' || usr.nomuser);
                HTP.tableOpen;
                    HTP.formOpen(owa_util.get_owa_service_path || 'ui_user.execedituseraccount', 'POST');
                        HTP.tableHeader('');
                        HTP.tableHeader('');

                        HTP.tableRowOpen;
                            HTP.tableData('Pseudonyme');
                            HTP.tableData(usr.pseudouser);
                        HTP.tableRowClose;

                        HTP.tableRowOpen;
                            HTP.tableData('Nom');
                            HTP.tableData(HTF.formText(cname => 'vnom', cvalue => usr.nomuser));
                        HTP.tableRowClose;

                        HTP.tableRowOpen;
                            HTP.tableData('Prénom');
                            HTP.tableData(HTF.formText(cname => 'vprenom', cvalue => usr.prenomuser));
                        HTP.tableRowClose;

                        HTP.tableRowOpen;
                            HTP.tableData('Numéro de téléphone');
                            HTP.tableData(HTF.formText(cname => 'vtel', cvalue => usr.teluser));
                        HTP.tableRowClose;

                        HTP.tableRowOpen;
                            HTP.tableData('Adresse Mail');
                            HTP.tableData(HTF.formText(cname => 'vmail', cvalue => usr.adrmail));
                        HTP.tableRowClose;

                        HTP.tableRowOpen;
                            HTP.tableData('Mot de passe');
                            HTP.tableData(HTF.formPassword(cname => 'vpassword'));
                        HTP.tableRowClose;
                        
                         HTP.tableRowOpen;
                            HTP.tableData(HTF.formSubmit(cvalue => 'Appliquer mes modifications'), cattributes => 'class="no-border"');
                            HTP.tableData('', cattributes => 'class="no-border"');
                        HTP.tableRowClose;
                    HTP.formClose;

                    HTP.formOpen(owa_util.get_owa_service_path || 'ui_user.showuseraccount', 'POST');
                        HTP.tableRowOpen;
                            HTP.tableData(HTF.formSubmit(cvalue => 'Annuler'), cattributes => 'class="no-border"');
                            HTP.tableData('', cattributes => 'class="no-border"');
                        HTP.tableRowClose;  
                    HTP.formClose;

                HTP.tableClose;
                ui_common.footer;
            HTP.bodyClose;
        HTP.htmlClose;
    END;

    PROCEDURE ExecEditUserAccount (
        vnom IN VARCHAR2, vprenom IN VARCHAR2, vtel IN VARCHAR2, 
        vmail IN VARCHAR2, vpassword IN VARCHAR2 DEFAULT NULL
    )
    IS
        usr utilisateur%ROWTYPE;
        prevMail utilisateur.adrmail%TYPE := NULL;
        mailToSet utilisateur.adrmail%TYPE := vmail;
        telToSet utilisateur.teluser%TYPE := vtel;
    BEGIN
        -- get connected user
        usr := pa_utils.get_user_connected();

        -- check password
        IF pa_utils.encrypt(vpassword) = usr.mdpuser THEN
            -- create the new email adress       
            IF vmail != usr.adrmail AND NOT pa_adresse_mail.Exist(vmail) AND pa_utils.check_mail(vmail) THEN
                pa_adresse_mail.Add(vmail, usr.numuser, 'Attente');
                prevMail := usr.adrmail;
            ELSIF vmail != usr.adrmail AND pa_adresse_mail.Exist(vmail) THEN
                mailToSet := usr.adrmail;
            END IF;    

            IF vtel != usr.teluser AND NOT pa_utils.check_tel(vtel) THEN
                telToSet := usr.teluser;
            END IF;

            -- update user profil
            pa_utilisateur.Upd(usr.numuser, mailToSet, vnom, vprenom, telToSet);

            -- remove the previous email adress from DB 
            IF prevMail IS NOT NULL THEN
                pa_adresse_mail.Del(prevMail);
            END IF;
        ELSE
            ui_common.redirect(owa_util.get_owa_service_path || 'ui_user.formedituseraccount');
        END IF;
      
        -- redirect
        ui_common.redirect(owa_util.get_owa_service_path || 'ui_user.showuseraccount');
    END;
END;
/

exit;
