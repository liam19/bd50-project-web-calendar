-- -----------------------------------------------------------------------------
--       TABLE : THEME
-- -----------------------------------------------------------------------------
CREATE TABLE THEME (
    NUMTHEME NUMBER(3) NOT NULL,
    NUMUSER NUMBER(5) NOT NULL,
    LIBTHEME VARCHAR2(20) NOT NULL,
    TYPETHEME VARCHAR2(20) NOT NULL,
	CONSTRAINT PK_THEME PRIMARY KEY (NUMTHEME)
) 
ORGANIZATION INDEX
TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : ROLE_UTILISATEUR
-- -----------------------------------------------------------------------------
CREATE TABLE ROLE_UTILISATEUR (
    NUMROLE NUMBER(2) NOT NULL,
    NOMROLE VARCHAR2(20) NOT NULL,
	CONSTRAINT PK_ROLE_UTILISATEUR PRIMARY KEY (NUMROLE)
) 
ORGANIZATION INDEX
TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : JOUR
-- -----------------------------------------------------------------------------
CREATE TABLE JOUR (
    IDJOUR NUMBER(1) NOT NULL,
    NOMJOUR VARCHAR2(10) NOT NULL,
	CONSTRAINT PK_JOUR PRIMARY KEY (IDJOUR)
) 
ORGANIZATION INDEX
TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : CONCERNER
-- -----------------------------------------------------------------------------
CREATE TABLE CONCERNER (
    NUMPERIOD NUMBER(3) NOT NULL,
    IDJOUR NUMBER(1) NOT NULL,
	CONSTRAINT PK_CONCERNER PRIMARY KEY (NUMPERIOD, IDJOUR)
) 
ORGANIZATION INDEX
TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : PARTAGER
-- -----------------------------------------------------------------------------
CREATE TABLE PARTAGER (
    NUMUSER NUMBER(5) NOT NULL,
    NUMEVT NUMBER(5) NOT NULL,
    NUMOCCUR NUMBER(4) NOT NULL,
    STATPRT VARCHAR2(10) NULL,
	CONSTRAINT PK_PARTAGER PRIMARY KEY (NUMUSER, NUMEVT, NUMOCCUR)
) 
ORGANIZATION INDEX
TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : CARACTERISER
-- -----------------------------------------------------------------------------
CREATE TABLE CARACTERISER (
    NUMTHEME NUMBER(3) NOT NULL,
    NUMEVT NUMBER(5) NOT NULL,
	CONSTRAINT PK_CARACTERISER PRIMARY KEY (NUMTHEME, NUMEVT)
) 
ORGANIZATION INDEX
TABLESPACE BD50_DATA;

exit;
