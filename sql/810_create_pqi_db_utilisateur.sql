create or replace
PACKAGE PA_UTILISATEUR
AS
    -- Ref Cursor
    TYPE c_type_utilisateur IS REF CURSOR;

    -- Insert
    PROCEDURE Add(
        in_adrmail IN VARCHAR2, in_numrole IN NUMBER, in_pseudouser IN VARCHAR2,
        in_mdpuser IN VARCHAR2, in_nomuser IN VARCHAR2, in_prenomuser IN VARCHAR2,
        in_statutuser IN VARCHAR2, in_teluser IN VARCHAR2
    );

    -- Get
    FUNCTION Get(in_numuser IN VARCHAR2) RETURN utilisateur%ROWTYPE;

    -- Get by pseudo
    FUNCTION GetByPseudo(in_pseudo IN VARCHAR2) RETURN utilisateur%ROWTYPE;

    -- Get by adrmail
    FUNCTION GetByAdrmail(in_adrmail IN VARCHAR2) RETURN utilisateur%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_utilisateur;

    -- Update
    PROCEDURE Upd(
        in_numuser IN VARCHAR2, in_adrmail IN VARCHAR2, in_nomuser IN VARCHAR2, 
        in_prenomuser IN VARCHAR2, in_teluser IN VARCHAR2
    );

    -- Delete
    PROCEDURE Del(in_numuser IN VARCHAR2);
END;
/

exit;
