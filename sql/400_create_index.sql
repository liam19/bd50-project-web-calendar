-- -----------------------------------------------------------------------------
--       CREATE INDEX : PSEUDOUSER
-- -----------------------------------------------------------------------------

CREATE UNIQUE INDEX I_PSEUDO
    ON UTILISATEUR (PSEUDOUSER ASC);

exit;
