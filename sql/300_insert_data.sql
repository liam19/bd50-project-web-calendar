CONNECT G06_WEBCAL/WEBCAL;

-- Insert default ROLE_UTILISATEUR
INSERT INTO role_utilisateur
VALUES (1, 'Admin');

INSERT INTO role_utilisateur
VALUES (2, 'User');

-- Insert default LOCALISATION
INSERT INTO localisation
VALUES (1, 'Localiation par défaut', 'Rue par défaut', 90000, 'Belfort', 'France');

-- Insert default PERIODICITE
INSERT INTO periodicite
VALUES (0, 'Period', '01/10/2020', '01/10/2020', 1, 1);

EXIT;
