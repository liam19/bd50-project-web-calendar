create or replace
PACKAGE BODY UI_COMMON AS

	-- header de la page
	PROCEDURE header AS
		pers_connected utilisateur%rowtype;
	BEGIN
		-- récupération de l'identité de la personne connectée
		pers_connected := pa_utils.get_user_connected;

		-- Start of Page Header --
		HTP.print( '<DIV id="page_header">' );

			HTP.print( '<DIV id="page_heading">' );
				HTP.header( 1, '<SPAN>' || 'Web Agenda' || '</SPAN>');
			HTP.print( '</DIV>' );

			HTP.print( '<DIV id="page_headerlinks">' );
				HTP.ulistOpen;
				HTP.listItem( 'Bienvenue&' || 'nbsp;&' || 'nbsp;' || pers_connected.prenomuser);
				HTP.listItem( htf.anchor( 'ui_user.showuseraccount', 'Mon profil'));
				HTP.listItem( htf.anchor( 'ui_calendar.eventlist', 'Mon Calendrier'));
				HTP.listItem( htf.anchor( 'ui_event.formcreateanevent', 'Créer un événement'));
				HTP.listItem( htf.anchor( 'ui_contact.contactlist', 'Gérer les contacts'));
				HTP.listItem( htf.anchor( 'pa_utils.disconnect_user', 'Déconnecter'), cattributes => ' class="last"');
				HTP.ulistClose;
			HTP.print( '</DIV>' );

			HTP.print('<div class="clearthis"> </div>');
		HTP.print( '</DIV>' );
		-- End of Page Header --
	END header;

	-- bas de page
	PROCEDURE footer AS
	BEGIN
		-- Start of Page Footer --
		HTP.print( '<DIV id="page_footer">' );
			HTP.print( '<DIV id="powered_by">' );
				HTP.print( 'Powered by ' );
				HTP.anchor( 'http://www.utbm.fr', 'UTBM - BD50');
			HTP.print( '</DIV>' );
			HTP.print('<div class="clearthis"> </div>');
		HTP.print( '</DIV>' );
		-- End of Page Footer --
	END footer;

	PROCEDURE err_message (err IN VARCHAR2, redirect IN VARCHAR2)
	AS
    BEGIN
		HTP.htmlOpen;
        HTP.headOpen;
            HTP.title('Page de création de compte');
            -- HTP.print('<link href="public/G06_WEBCAL/css/calendar.css" rel="stylesheet" type="text/css" />');
        HTP.headClose;
        HTP.bodyOpen;
			-- print error
			HTP.print(err);
			-- redirect 
			HTP.formOpen(owa_util.get_owa_service_path || redirect, 'POST');
				HTP.formSubmit(cvalue => 'Retour');
			HTP.formClose;
        HTP.bodyClose;
        HTP.htmlClose;
    END;

		-- redirect to another page
	PROCEDURE redirect (v_link varchar2, v_time number default 0)
	IS
	BEGIN
		HTP.print('<script language="javascript" type="text/javascript">');
		HTP.print('<!-- ');
    	HTP.print('window.SetTimeOut(window.location="' || v_link || '", ' || v_time ||');');
		HTP.print('-->');
		HTP.print('</script>');
	END;

END UI_COMMON;
/

exit;
