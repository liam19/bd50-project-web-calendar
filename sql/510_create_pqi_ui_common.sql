create or replace
PACKAGE UI_COMMON AS
	-- header
	procedure header;

	-- bas de page
	procedure footer;

	-- print error message, then redirect
	procedure err_message(err IN VARCHAR2, redirect IN VARCHAR2);

	-- redirect to another page
  	PROCEDURE redirect (v_link varchar2, v_time number default 0);

END UI_COMMON;
/

exit;
