-- -----------------------------------------------------------------------------
--       TABLE : OCCURRENCE
-- -----------------------------------------------------------------------------
ALTER TABLE
    OCCURRENCE
ADD
    CONSTRAINT PK_OCCURRENCE PRIMARY KEY (NUMEVT, NUMOCCUR);

-- -----------------------------------------------------------------------------
--       TABLE : UTILISATEUR
-- -----------------------------------------------------------------------------
ALTER TABLE
    UTILISATEUR
ADD
    CONSTRAINT PK_UTILISATEUR PRIMARY KEY (NUMUSER);

-- -----------------------------------------------------------------------------
--       TABLE : ADRESSE_MAIL
-- -----------------------------------------------------------------------------
ALTER TABLE
    ADRESSE_MAIL
ADD
    CONSTRAINT PK_ADRESSE_MAIL PRIMARY KEY (ADRMAIL);

-- -----------------------------------------------------------------------------
--       TABLE : LOCALISATION
-- -----------------------------------------------------------------------------
ALTER TABLE
    LOCALISATION
ADD
    CONSTRAINT PK_LOCALISATION PRIMARY KEY (NUMLOC);

-- -----------------------------------------------------------------------------
--       TABLE : EVENEMENT
-- -----------------------------------------------------------------------------
ALTER TABLE
    EVENEMENT
ADD
    CONSTRAINT PK_EVENEMENT PRIMARY KEY (NUMEVT);

-- -----------------------------------------------------------------------------
--       TABLE : PERIODICITE
-- -----------------------------------------------------------------------------
ALTER TABLE
    PERIODICITE
ADD
    CONSTRAINT PK_PERIODICITE PRIMARY KEY (NUMPERIOD);

-- -----------------------------------------------------------------------------
--       TABLE : CONTACT
-- -----------------------------------------------------------------------------
ALTER TABLE
    CONTACT
ADD
    CONSTRAINT PK_CONTACT PRIMARY KEY (NUMUSER, NUMCONTACT);

exit;
