grant connect, resource, create view, plustrace, create synonym, create user, alter user, xdbadmin to G06_WEBCAL with admin option;
grant execute on dbms_epg to G06_WEBCAL with grant option;
grant execute on dbms_xdb to G06_WEBCAL with grant option;

exit;
