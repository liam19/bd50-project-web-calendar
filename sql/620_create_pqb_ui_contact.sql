create or replace
PACKAGE BODY UI_CONTACT
AS
    -- Event List Page
    PROCEDURE ContactList
    IS
        usr utilisateur%ROWTYPE;
        contacts SYS_REFCURSOR;
        contact utilisateur%ROWTYPE;
    BEGIN
        -- get connect user
        usr := pa_utils.get_user_connected();

        -- get current user's contacts
        contacts := pa_contact.GetAllContacts(usr.numuser);

        HTP.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        HTP.htmlOpen;
        HTP.headOpen;
        HTP.title( 'Contacts');
        HTP.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        HTP.headClose;
        HTP.bodyOpen;
           
        ui_common.header;
        HTP.header(1, 'Contacts');
        HTP.tableOpen;

            HTP.tableHeader('Pseudonyme');
            HTP.tableHeader('Nom');
            HTP.tableHeader('Adresse Mail');
            HTP.formOpen(owa_util.get_owa_service_path || 'ui_contact.formaddcontact', 'POST');
                HTP.tableHeader(HTF.formSubmit(cvalue => 'Ajouter'), cattributes => 'class="no-border"');
            HTP.formClose;

            LOOP
                FETCH contacts INTO contact;
                EXIT WHEN contacts%NOTFOUND;
                HTP.tableRowOpen;
                    HTP.tableData(contact.pseudouser);
                    HTP.tableData(contact.prenomuser || ' ' || contact.nomuser);
                    HTP.tableData(contact.adrmail);  
                    HTP.formOpen(owa_util.get_owa_service_path || 'ui_contact.execdeletecontact?in_numcontact=' || contact.numuser, 'POST');
                        HTP.tableHeader(HTF.formSubmit(cvalue => 'Supprimer'), cattributes => 'class="no-border"');
                    HTP.formClose;                  
                HTP.tableRowClose;
            END LOOP;

        HTP.tableClose;
        ui_common.footer;

        HTP.bodyClose;
        HTP.htmlClose;

    EXCEPTION
        WHEN no_data_found THEN
            ui_common.redirect('ui_calendar.eventlist');
    END;

    -- Add Contact Page
    PROCEDURE FormAddContact
    IS
    BEGIN
        HTP.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        HTP.htmlOpen;
        HTP.headOpen;
        HTP.title( 'Ajout d''un contact');
        HTP.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        HTP.headClose;
        HTP.bodyOpen;
           
        ui_common.header;
        HTP.header(1, 'Ajouter un contact');
        HTP.print('Ajouter un contact par pseudo ou par adresse email.');
        HTP.formOpen(owa_util.get_owa_service_path || 'ui_contact.execaddcontact', 'POST');
            HTP.formText('videntification');
            HTP.formSubmit(cvalue => 'Ajouter');
        HTP.formClose;

        ui_common.footer;

        HTP.bodyClose;
        HTP.htmlClose;
    END;

    PROCEDURE ExecAddContact(videntification IN VARCHAR2)
    IS
        usr utilisateur%ROWTYPE;
        contact utilisateur%ROWTYPE;
    BEGIN
        -- get connect user
        usr := pa_utils.get_user_connected();

        -- check if videntification is an email adress
        IF pa_utils.check_mail(videntification) THEN
            contact := pa_utilisateur.GetByAdrmail(videntification);
        ELSE -- else, it's a pseudo
            contact := pa_utilisateur.GetByPseudo(videntification);
        END IF;

        -- create the contact
        pa_contact.Add(usr.numuser, contact.numuser);

        -- redirect
        ui_common.redirect('ui_contact.contactlist');

    EXCEPTION
        WHEN others THEN
            ui_common.err_message('Erreur : Utilisateur introuvable.', 'ui_contact.formaddcontact');
    END;

    -- Remove Contact
    PROCEDURE ExecDeleteContact(in_numcontact IN NUMBER)
    IS
        usr utilisateur%ROWTYPE;
    BEGIN
        -- get connected user
        usr := pa_utils.get_user_connected();

        -- remove the contact
        pa_contact.Del(usr.numuser, in_numcontact);

        -- redirect
        ui_common.redirect('ui_contact.contactlist');
    END;    
END;
/

exit;
