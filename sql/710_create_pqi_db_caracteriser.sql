CREATE OR REPLACE PACKAGE PA_CARACTERISER
AS
    -- Ref Cursor
    TYPE c_type_caract IS REF CURSOR;

    -- Insert
    PROCEDURE Add(in_numtheme IN NUMBER, in_numevt IN NUMBER);

    -- Get
    -- FUNCTION Get(in_adrmail IN VARCHAR2) RETURN caracteriser%ROWTYPE;

    -- GetAll
    FUNCTION GetAll RETURN c_type_caract;

    -- Delete
    PROCEDURE Del(in_numtheme IN NUMBER, in_numevt IN NUMBER);
END;
/

exit;
