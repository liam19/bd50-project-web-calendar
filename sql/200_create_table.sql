-- -----------------------------------------------------------------------------
--       TABLE : OCCURRENCE
-- -----------------------------------------------------------------------------
CREATE TABLE OCCURRENCE (
   NUMEVT NUMBER(5) NOT NULL,
   NUMOCCUR NUMBER(4) NOT NULL,
   NUMLOC NUMBER(4) NOT NULL,
   DEBUTOCCUR DATE NOT NULL,
   FINOCCUR DATE NOT NULL,
   INFOCCUR VARCHAR2(255) NULL,
   STATOCCUR VARCHAR2(10) NULL
) TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : UTILISATEUR
-- -----------------------------------------------------------------------------
CREATE TABLE UTILISATEUR (
   NUMUSER NUMBER(5) NOT NULL,
   ADRMAIL VARCHAR2(50) NOT NULL,
   NUMROLE NUMBER(2) NOT NULL,
   PSEUDOUSER VARCHAR2(20) NOT NULL,
   MDPUSER VARCHAR2(40) NOT NULL,
   NOMUSER VARCHAR2(20) NOT NULL,
   PRENOMUSER VARCHAR2(20) NOT NULL,
   STATUTUSER VARCHAR2(20) NOT NULL,
   TELUSER VARCHAR2(11) NULL
) TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : ADRESSE_MAIL
-- -----------------------------------------------------------------------------
CREATE TABLE ADRESSE_MAIL (
   ADRMAIL VARCHAR2(50) NOT NULL,
   NUMUSER NUMBER(5) NULL,
   STATMAIL VARCHAR2(10) NULL
) TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : LOCALISATION
-- -----------------------------------------------------------------------------
CREATE TABLE LOCALISATION (
   NUMLOC NUMBER(4) NOT NULL,
   DESCRLOC VARCHAR2(128) NOT NULL,
   RUELOC VARCHAR2(128) NULL,
   CPLOC VARCHAR2(8) NULL,
   VILLELOC VARCHAR2(30) NULL,
   PAYSLOC VARCHAR2(30) NULL
) TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : EVENEMENT
-- -----------------------------------------------------------------------------
CREATE TABLE EVENEMENT (
   NUMEVT NUMBER(5) NOT NULL,
   NUMPERIOD NUMBER(3) NULL,
   NUMUSER NUMBER(5) NOT NULL,
   TITREEVT VARCHAR2(32) NOT NULL,
   DISPOEVT NUMBER(1) NOT NULL,
   DESCREVT VARCHAR2(255) NULL
) TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : PERIODICITE
-- -----------------------------------------------------------------------------
CREATE TABLE PERIODICITE (
   NUMPERIOD NUMBER(3) NOT NULL,
   TYPEPERIOD VARCHAR2(10) NOT NULL,
   DEBUTPERIODIC DATE NOT NULL,
   FINPERIODIC DATE NOT NULL,
   NBOCCUR NUMBER(4) DEFAULT 1 NOT NULL,
   FREQPERIOD NUMBER(2) NOT NULL
) TABLESPACE BD50_DATA;

-- -----------------------------------------------------------------------------
--       TABLE : CONTACT
-- -----------------------------------------------------------------------------
CREATE TABLE CONTACT (
   NUMUSER NUMBER(5) NOT NULL,
   NUMCONTACT NUMBER(5) NOT NULL
) TABLESPACE BD50_DATA;

exit;
