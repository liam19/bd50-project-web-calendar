create or replace
PACKAGE BODY UI_EVENT
AS
    -- Event List Page
    PROCEDURE FormCreateAnEvent
    IS
    BEGIN
        htp.print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict">');
        htp.htmlOpen;
        htp.headOpen;
        htp.title( 'Calendrier');
        htp.linkrel('stylesheet', '/public/G06_WEBCAL/css/style.css', null);
        htp.headClose;
        htp.bodyOpen;
           
        ui_common.header;
            HTP.header(1, 'Création d''un evenement');
            HTP.formOpen(owa_util.get_owa_service_path || 'ui_event.execcreateanevent', 'POST');
                HTP.tableOpen;
                    HTP.tableHeader('');
                    HTP.tableHeader('');
                    HTP.tableRowOpen;
                        HTP.tableData('Titre');
                        HTP.tableData(HTF.formText('vtitreevt'));
                    HTP.tableRowClose;  

					HTP.tableRowOpen;
                        HTP.tableData('Disponibilité :');
                    HTP.tableRowClose;  
					
					HTP.tableRowOpen;
                        HTP.tableData('Vrai');
                        HTP.tableData(HTF.formRadio('vdispoevt', 1, null, null));
                    HTP.tableRowClose;  
					
					HTP.tableRowOpen;
                        HTP.tableData('Faux');
                        HTP.tableData(HTF.formRadio('vdispoevt', 0, null, null));
                    HTP.tableRowClose; 

                    HTP.tableRowOpen;
                        HTP.tableData('Description');
                        HTP.tableData(HTF.formText('vdescrevt'));
                    HTP.tableRowClose; 
					
					HTP.tableRowOpen;
					HTP.tableRowClose;
					HTP.tableRowOpen;
					HTP.tableRowClose;
					
					HTP.tableRowOpen;
                        HTP.tableData('Date de début');
                        HTP.tableData(HTF.formText('vdebutoccur'));
                    HTP.tableRowClose; 
					
					HTP.tableRowOpen;
                        HTP.tableData('Date de fin');
                        HTP.tableData(HTF.formText('vfinoccur'));
                    HTP.tableRowClose; 
					
					HTP.tableRowOpen;
                        HTP.tableData('Information occurrence');
                        HTP.tableData(HTF.formText('vinfooccur'));
                    HTP.tableRowClose; 
					
					HTP.tableRowOpen;
                        HTP.tableData('Statut occurrence');
                        HTP.tableData(HTF.formText('vstatoccur'));
                    HTP.tableRowClose; 

                    HTP.tableRowOpen;
                        HTP.tableData('', cattributes => 'class="no-border"');
                        HTP.tableData(HTF.formSubmit(cvalue => 'Créer l''évenement'), cattributes => 'class="no-border"');
                    HTP.tableRowClose;

                HTP.tableClose;
            HTP.formClose;
            ui_common.footer;
        HTP.bodyClose;
        HTP.htmlClose;
    EXCEPTION
        WHEN others THEN
            ui_common.redirect('ui_auth.formconnectuser');
    END;   

	-- Create an event
    PROCEDURE ExecCreateAnEvent 
	(
        vtitreevt IN VARCHAR2, vdispoevt IN NUMBER, vdescrevt IN VARCHAR2, 
		vdebutoccur IN DATE, vfinoccur IN DATE, vinfooccur in VARCHAR2, vstatoccur in VARCHAR2
    )
    IS
		num_evenement NUMBER;
        usr utilisateur%ROWTYPE;
    BEGIN
        -- get user by ID
        usr := pa_utils.get_user_connected();
		
        pa_evenement.Add(0, usr.numuser, vtitreevt, vdispoevt, vdescrevt);
		
        SELECT EVENEMENT.NUMEVT INTO num_evenement FROM EVENEMENT 
					WHERE EVENEMENT.TITREEVT = vtitreevt;
		
		pa_occurrence.Add(num_evenement, 1, to_date(vdebutoccur, 'dd/mm/yyyy'), to_date(vfinoccur, 'dd/mm/yyyy'), vinfooccur, vstatoccur);
		ui_common.redirect('ui_calendar.eventlist');
    EXCEPTION
        WHEN others THEN
            ui_common.err_message('Erreur lors de la création de l''événement.', 'ui_event.formcreateanevent');
    END;

    PROCEDURE ExecDeleteEvent(in_numoccur IN NUMBER)
    IS
        v_numevt NUMBER;
    BEGIN
        -- get event number
        v_numevt := pa_occurrence.GetEventNumberByOccurrence(in_numoccur);

        -- delete the occurrence first
        pa_occurrence.Del(v_numevt, in_numoccur);

        -- if no more occurrence linked to the event, then delete the event
        IF pa_occurrence.CountOccurrencesByEvent(v_numevt) = 0 THEN
            pa_evenement.Del(v_numevt);
        END IF;

        -- redirect
        ui_common.redirect('ui_calendar.eventlist');
    EXCEPTION
        WHEN others THEN
            ui_common.err_message('Erreur lors de la suppression de l''événement.', 'ui_calendar.eventlist');
    END;
	
END;
/

exit;
