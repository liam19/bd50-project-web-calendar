create or replace
PACKAGE UI_EVENT
AS
    -- Event List Page
    PROCEDURE FormCreateAnEvent;
	
	-- Add an Event (and an occurrence)
	PROCEDURE ExecCreateAnEvent 
	(
        vtitreevt IN VARCHAR2, vdispoevt IN NUMBER, vdescrevt IN VARCHAR2,
		vdebutoccur IN DATE, vfinoccur IN DATE, vinfooccur in VARCHAR2, vstatoccur in VARCHAR2
    );

    -- Remove an occurrence (and the event if necessary)
    PROCEDURE ExecDeleteEvent(in_numoccur IN NUMBER);
END;
/

exit;
